FROM python:2.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /missionbe
WORKDIR /missionbe
ADD requirements.txt /missionbe/
RUN pip install -r requirements.txt
ADD . /missionbe/