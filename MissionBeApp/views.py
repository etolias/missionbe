from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
import json
from django.core import serializers
from .models import *

# Create your views here.
from django.http import HttpResponse

def populate_context(request):
    loggedIn = request.user.is_authenticated()
    loggedInUser = request.user.username.split("@")[0]
    context = { 
               'MEDIA_URL': settings.STATIC_URL, 
               'ROOT_PATH_PREFIX': 'http://127.0.0.1:8000/',
               'loggedIn': loggedIn,
               'LOGGED_IN_USER': loggedInUser
               }
    return context


def index(request):
    context = populate_context(request)
    return render_to_response('index.html', context, context_instance=RequestContext(request))


def mindful(request):
    context = populate_context(request)
    page_title = request.path.rsplit('/', 1)[-2]
    context.update({'page_title': 'mindful'})
    content_list = Object.objects.all().filter(object_type=page_title)
    context.update({'content_list': content_list})
    return render_to_response('mindful.html', context, context_instance=RequestContext(request))


def breathe(request):
    context = populate_context(request)
    page_title = request.path.split('/')[-2]
    context.update({'page_title': page_title})
    content_list = Object.objects.all().filter(object_type=page_title)
    context.update({'content_list': content_list})
    return render_to_response('lesson.html', context, context_instance=RequestContext(request))

def envision(request):
    context = populate_context(request)
    page_title = request.path.split('/', 1)[-2]
    context.update({'page_title': page_title})
    content_list = Object.objects.all().filter(object_type=page_title)
    context.update({'content_list': content_list})
    return render_to_response('lesson.html', context, context_instance=RequestContext(request))

def timer(request):
    context = populate_context(request)
    page_title = request.path.rsplit('/', 1)[-2]
    context.update({'page_title': page_title})
    content_list = Object.objects.all().filter(object_type=page_title)
    context.update({'content_list': content_list})
    return render_to_response('lesson.html', context, context_instance=RequestContext(request))

def affirm(request):
    context = populate_context(request)
    page_title = request.path.rsplit('/', 1)[-2]
    context.update({'page_title': page_title})
    content_list = Object.objects.all().filter(object_type=page_title)
    context.update({'content_list': content_list})
    return render_to_response('lesson.html', context, context_instance=RequestContext(request))

def move(request):
    context = populate_context(request)
    page_title = request.path.rsplit('/', 1)[-2]
    context.update({'page_title': page_title})
    content_list = Object.objects.all().filter(object_type=page_title)
    context.update({'content_list': content_list})
    return render_to_response('lesson.html', context, context_instance=RequestContext(request))

def connect(request):
    page_title = request.path.rsplit('/', 1)[-2]
    context.update({'page_title': page_title})
    content_list = Object.objects.all().filter(object_type=page_title)
    context.update({'content_list': content_list})
    return render_to_response('lesson.html', context, context_instance=RequestContext(request))

