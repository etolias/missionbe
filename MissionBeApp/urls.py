from django.conf.urls import url

from . import views

urlpatterns = [

    url(r'^$', views.index, name='index'),
    url(r'^mindful/$', views.mindful, name='mindful'),
    url(r'^breathe/$', views.breathe, name='breathe'),
    url(r'^envision/$', views.envision, name='envision'),
    url(r'^timer/$', views.timer, name='timer'),
    url(r'^affirm/$', views.affirm, name='affirm'),
    url(r'^move/$', views.move, name='move'),
    url(r'^connect/$', views.connect, name='connect'),
]